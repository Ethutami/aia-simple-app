import React from 'react';
import Enzyme, {shallow} from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import LoginScreen from './login-screen';
Enzyme.configure({adapter: new Adapter()});

describe('login-screen', () => {
  it('should render Image', () => {
    const wrapper = shallow(<LoginScreen />);
    expect(wrapper.find('Image')).to.have.lengthOf(1);
  });

  it('renders <InputField/> component', () => {
    const wrapper = shallow(<LoginScreen />);
    expect(wrapper.find('<InputField/>')).to.have.lengthOf(2);
  });
  it('renders <Button/> component', () => {
    const wrapper = shallow(<LoginScreen />);
    expect(wrapper.find('<Button/>')).to.have.lengthOf(1);
  });

});
