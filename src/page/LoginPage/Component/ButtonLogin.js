import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';

export default function Button(props) {
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      style={styles.button}
      onPress={() => navigation.navigate(props.navigation)}>
      <Text style={styles.txt}>{props.label}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  button: {
    backgroundColor: '#630A10',
    borderRadius: 8,
    marginTop: 32,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
  },
  txt: {
    alignSelf: 'center',
    fontSize: 24,
    fontWeight: 'bold',
    color: '#ffffff',
  },
});
