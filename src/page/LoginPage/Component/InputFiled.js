import React from 'react';
import {StyleSheet, TextInput} from 'react-native';

export default function Input(props) {
  return (
    <TextInput
      style={styles.txt_input}
      placeholder={props.label}
      placeholderTextColor="#ED7782"
    />
  );
}

const styles = StyleSheet.create({
  txt_input: {
    backgroundColor: '#F3F3F3',
    borderRadius: 8,
    paddingLeft: '5%',
    marginTop: 32,
  },
});
