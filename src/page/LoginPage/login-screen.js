import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';

import InputField from './Component/InputFiled';
import Button from './Component/ButtonLogin';
import simpleAppLogo from '../../assets/simple-app-logo.png';

export default function LoginScreen({navigation}) {
  return (
    <View style={styles.container}>
      <Image source={simpleAppLogo} resizeMode="stretch" style={styles.img} />
      <View style={styles.form}>
        <InputField label="Username/Email" />
        <InputField label="Password" />
      </View>
      <View style={styles.button_container}>
        <Button label="Login" navigation="Home" />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: '20%',
    paddingHorizontal: '10%',
    width: '100%',
    backgroundColor: '#ffffff',
  },
  img: {width: 150, height: 150, alignSelf: 'center', marginBottom: 150/2},
  form: {width: '100%', marginTop: '10%'},
  button_container: {width: '100%', marginTop: '20%'},
});
