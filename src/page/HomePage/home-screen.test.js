import React from 'react';
import Enzyme, {shallow} from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import HomeScreen from './home-screen';
Enzyme.configure({adapter: new Adapter()});

describe('home-screen', () => {
  it('renders <ComponentSearchBar/> component', () => {
    const wrapper = shallow(<HomeScreen />);
    expect(wrapper.find('<ComponentSearchBar/>')).to.have.lengthOf(1);
  });
  it('renders <ComponetProductCard/> component', () => {
    const wrapper = shallow(<HomeScreen />);
    expect(wrapper.find('<ComponetProductCard/>')).to.have.lengthOf(1);
  });
});
