import React, {PureComponent} from 'react';
import {Image, Pressable, StyleSheet, Text, View} from 'react-native';
import {connect} from 'react-redux';

const mapStateToProps = state => {
  return {
    entries: state.product.entries,
  };
};

export class ComponetProductCard extends PureComponent {
  render() {
    return (
      <View style={styles.container}>
        {this.props.label === 'show' && (
          <View style={styles.wrap_label}>
            <Text style={styles.label_txt}>Activity</Text>
          </View>
        )}
        <View style={styles.entries_container}>
          {this.props.entries.map((item, index) => {
            return (
              <Pressable
                key={index}
                style={styles.entries_inner_containaer}
                onPress={() => this.props.navigation.navigate('DetailProduct')}>
                <Image
                  source={{uri: item?.media.m}}
                  style={styles.product_item}
                />
                <View style={{padding: 10}}>
                  <Text style={styles.author}>{item?.author_id}</Text>
                  <Text style={styles.published}>{item?.published}</Text>
                  <Text style={styles.title}>{item?.title}</Text>
                </View>
              </Pressable>
            );
          })}
        </View>
      </View>
    );
  }
}

export default connect(mapStateToProps)(ComponetProductCard);

const styles = StyleSheet.create({
  container: {marginBottom: 22, marginTop: 24},
  wrap_label: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 6,
  },
  label_txt: {
    color: '#012443',
    fontFamily: 'Mark_pro',
    fontWeight: 'bold',
    fontSize: 25,
  },
  view_all: {
    color: '#FF6B6B',
    fontSize: 15,
  },
  entries_container: {
    flex: 1,
    width: '100%',
    flexWrap: 'wrap',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  entries_inner_containaer: {
    width: '100%',
    backgroundColor: '#ffffff',
    borderRadius: 10,
    marginBottom: 10,
    paddingBottom: 8,
  },
  product_item: {
    width: '100%',
    height: 168,
    borderRadius: 10,
    alignSelf: 'center',
    marginBottom: 7,
  },
  author: {
    color: '#012443',
    fontSize: 10,
    fontWeight: '400',
  },
  published: {
    color: '#012443',
    fontSize: 16,
    fontWeight: '700',
  },
  title: {
    color: '#FDD2BF',
    fontSize: 12,
    fontWeight: '700',
  },
});
