import React, {PureComponent} from 'react';
import {

  View,
  StyleSheet,
  TouchableWithoutFeedback,
} from 'react-native';
import Antdesign from 'react-native-vector-icons/AntDesign';
import {connect} from 'react-redux';
import {TextInput} from 'react-native-gesture-handler';
import {SearchItem} from '../../../Redux/action/SearchItem';

const mapStateToProps = state => {
  return {
    entries: state.product.entries,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    searchItem: word => {
      try {
        dispatch(SearchItem(word));
      } catch (error) {}
    },
  };
};

export class ComponentSearchBar extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      switch_view: false,
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableWithoutFeedback>
          <View style={styles.wrap_search}>
            <Antdesign name="search1" color="#FF6B6B" size={16} />
            <TextInput
              placeholder="search"
              style={styles.search_txt}
              onChangeText={text => this.props.searchItem(text)}
            />
          </View>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ComponentSearchBar);

const styles = StyleSheet.create({
  container: {
    paddingTop: 14,
    paddingBottom: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  wrap_search: {
    width: '100%',
    height: 34,
    backgroundColor: '#ffffff',
    borderRadius: 50,
    paddingHorizontal: 24,
    flexDirection: 'row',
    alignItems: 'center',
    shadowColor: '#BFC5F5',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 2,
  },
  search_txt: {
    width: '100%',
    marginLeft: 12,
    color: 'rgba(1, 0, 53, 0.5)',
  },
});
