import React, {PureComponent, useEffect} from 'react';
import {StyleSheet, ScrollView} from 'react-native';
import {connect} from 'react-redux';
import {GetProductEntries} from '../../Redux/action/ProductAction';
import ComponetProductCard from './Componet/ProductCard';
import ComponentSearchBar from './Componet/Search';

const mapStateToProps = state => {
  return {
    state,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onAppear: () => {
      try {
        dispatch(GetProductEntries());
      } catch (error) {}
    },
  };
};

const HomeScreen = ({navigation, onAppear}) => {
  useEffect(() => {
    onAppear();
  }, []);
  return (
    <ScrollView style={styles.container}>
      <ComponentSearchBar navigation={navigation} />
      <ComponetProductCard label="show" navigation={navigation} />
    </ScrollView>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 10,
  },
});
