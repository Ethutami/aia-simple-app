import axios from 'axios';

export const SearchItem = text => async (dispatch, getState) => {
  const entries = !text
    ? await axios.get(
        'https://www.flickr.com/services/feeds/photos_public.gne?tagmode=tagmode=all&format=json&lang=en-us&nojsoncallback=true',
      )
    : getState().product.entries.filter(item => item.title.match(text));

  dispatch({
    type: 'GET_PRODUCT_ENTRIES',
    value: text ? entries : entries.data.items,
  });
};
