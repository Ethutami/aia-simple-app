import axios from 'axios';

export const GetProductEntries = () => async dispatch => {
  const entries = await axios.get(
    'https://www.flickr.com/services/feeds/photos_public.gne?tagmode=tagmode=all&format=json&lang=en-us&nojsoncallback=true',
  );
  dispatch({
    type: 'GET_PRODUCT_ENTRIES',
    value: entries.data.items,
  });
};
