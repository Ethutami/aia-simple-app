const initialState = {
  data: '',
  entries: [],
};

const Product = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_PRODUCT_ENTRIES':
      return {
        ...state,
        entries: action.value,
      };

    default:
      return state;
  }
};

export default Product;
